package pk.nu.weather_room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class final_weather_roomDB extends AppCompatActivity {

    public String cityName;
    public String countrydata;
    public String humidity;
    public String temp;
    public String windspeed;

    public int id;
    EditText t1,t2;
    TextView data;

    Button btn,fetch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_weather_room_db);


        t1 = (EditText)findViewById(R.id.room_text1);
        data = (TextView) findViewById(R.id.dataholder);
        btn = (Button)findViewById(R.id.room_btn);
        fetch = (Button)findViewById(R.id.fetch_btn);




        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        countrydata = extras.getString("country");
        cityName = extras.getString("city");
        temp = extras.getString("temprature");
        windspeed = extras.getString("windspeed");
        humidity = extras.getString("humidity");

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new bgthread().start();
            }
        });

        fetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(final_weather_roomDB.this,finaloutput.class));


            }
        });

    }


    class bgthread extends Thread{
        public void run(){
            super.run();
            id = Integer.parseInt(t1.getText().toString());
            AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                    AppDatabase.class, "databasenew").build();
            UserDao userDao = db.userDao();
            userDao.insertrecord(new Userweather( id,countrydata,cityName,temp,windspeed,humidity));

            t1.setText("");
        //    t2.setText("");

            }
    }



}