package pk.nu.weather_room;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity {

    private Button weather;

    TextInputLayout name;

    private String  weatherCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        name =  (TextInputLayout) findViewById(R.id.name);
        weather = (Button)findViewById(R.id.weatherbtn);


        weather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                weatherCity =name.getEditText().getText().toString();


                Intent setData = new Intent(MainActivity.this, WeatherData.class);
                setData.putExtra("city",weatherCity);
                startActivity(setData);
            }
        });


    }
}