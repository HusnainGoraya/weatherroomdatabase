package pk.nu.weather_room;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Userweather {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "Country")
    public String country;

    @ColumnInfo(name = "City")
    public String city;

    @ColumnInfo(name = "Temprature")
    public String temprature;

    @ColumnInfo(name = "Wind Speed")
    public String speed;

    @ColumnInfo(name = "Humidity")
    public String humidity;

      public Userweather(int uid, String country, String city, String temprature, String speed, String humidity) {
        this.uid = uid;
        this.country = country;
        this.city = city;
        this.temprature = temprature;
        this.speed = speed;
        this.humidity = humidity;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTemprature() {
        return temprature;
    }

    public void setTemprature(String temprature) {
        this.temprature = temprature;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

}
