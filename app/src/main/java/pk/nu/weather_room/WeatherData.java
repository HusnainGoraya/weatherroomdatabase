package pk.nu.weather_room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class WeatherData extends AppCompatActivity {


    public TextView result;
    public TextView country1;
    public TextView city1;
    public TextView humidity1;
    public TextView windspeed1;
    public Button savedata;
    public String cityName;
    public String countrydata;
    public String cityfinal;
    public String humidity;
    public Double temp;


    public String speed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_data);
        Intent intent = getIntent();
         cityName = intent.getStringExtra("city");


        result = findViewById(R.id.desctemp);
        country1 = findViewById(R.id.country);
        city1 = findViewById(R.id.city);
        humidity1 = findViewById(R.id.humidity);
        windspeed1 = findViewById(R.id.wind);
        savedata = (Button)findViewById(R.id.room_btn);





        gotoFetch();


        savedata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saverecord();
            }
        });



    }

    public void gotoFetch(){
        String api_key = "e269df22896e7a3582c6a5a2935d0521";

        String url = "https://api.openweathermap.org/data/2.5/weather?q="+cityName+"&appid=e269df22896e7a3582c6a5a2935d0521";

        RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {


                    JSONObject windres = response.getJSONObject("wind");
                        speed = windres.getString("speed");

                    windspeed1.setText("Speed :"+speed);


                    JSONObject cou = response.getJSONObject("sys");
                  countrydata = cou.getString("country");
                    country1.setText("Country :"+countrydata);


                      cityfinal = response.getString("name");
                    city1.setText("city :"+cityfinal);

                    JSONObject object = response.getJSONObject("main");
                    String temprature = object.getString("temp");
                      temp = Double.parseDouble(temprature)-273.15;

                    humidity = object.getString("humidity");

                    result.setText("Temprature : "+temp.toString().substring(0,5)+"C");
                    humidity1.setText("Humidity :"+ humidity);


                } catch (JSONException e) {
                    result.setText(e.getMessage());

                    Toast.makeText(WeatherData.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                result.setText(error.toString());

                Toast.makeText(WeatherData.this, error.toString(), Toast.LENGTH_SHORT).show();

            }
        });

        queue.add(request);

    }

    public void saverecord() {

        Intent intent = new Intent(WeatherData.this, final_weather_roomDB.class);
        Bundle extras = new Bundle();
        extras.putString("country", countrydata);
        extras.putString("city", cityfinal);
        extras.putString("temprature", temp.toString().substring(0, 5));
        extras.putString("windspeed", speed);
        extras.putString("humidity", humidity);

        intent.putExtras(extras);
        Toast.makeText(WeatherData.this, "Go to Next", Toast.LENGTH_SHORT).show();
        startActivity(intent);


    }
//
//        public void save(){
//                AppDatabase db = Room.databaseBuilder(getApplicationContext(),
//                        AppDatabase.class, "Final").build();
//
//                UserDao userDao = db.userDao();
//
//                userDao.insert(new User(1,"countrydata","cityfinal"));
//
//
//               Toast.makeText( getApplicationContext(), "Room Data Inserted", Toast.LENGTH_SHORT).show();
//
//            }

        }
