package pk.nu.weather_room;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Userweather.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
}
