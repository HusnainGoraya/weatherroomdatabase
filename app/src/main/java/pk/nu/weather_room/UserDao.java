package pk.nu.weather_room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserDao {

    @Insert
    void insertrecord(Userweather  users);

    @Query("SELECT * FROM Userweather" )
    List<Userweather> getalldata();

}

